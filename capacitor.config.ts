import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starterioncap',
  appName: 'cap-test',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
