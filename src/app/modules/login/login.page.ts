import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { BaseService } from 'src/app/core/base.service';
import { LocalStorageService } from 'src/app/core/utils/local-storage.service';
import { SessionData } from 'src/app/core/utils/sessions-keys';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
 // form: FormGroup;
 // logform: FormGroup;
 email:any='';
 name:any='';
 closeResult: string = '';
 submitted = false;
 submittednw = false;
 mpin:any='';
 error_message='';
  username:any='';
  password:any='';
  showLoader = false;
  constructor(
    private localStorageService: LocalStorageService,
    private modalCtrl: ModalController,
    private alertController: AlertController,
    private _baseServeice: BaseService,
    private toastController: ToastController,
    public loadingCtrl: LoadingController,
    private fb: FormBuilder,
    private formBuilder: FormBuilder ,
    private router: Router,) { }

  ngOnInit() {
   
    this.localStorageService.getItem('userId').then(data => {
      //console.log(data.length);
      
    });
  }
  async  presentToast(msg:any) {
    let toast =await  this.toastController.create({
      message: msg,
      duration: 5000,
      position: "top",
      color:'danger',
      cssClass: 'toast-custom-class',
    });

   
    toast.present();
  }
  async login() {
    let body='';
    let loading =await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    const usernamealert = await this.alertController.create({
             
      message: 'Enter your username',
      buttons: ['OK']
    });
    const passalert = await this.alertController.create({
             
      message: 'Enter your password',
      buttons: ['OK']
    });
   if(!this.username){
    
    await usernamealert.present();
   }else if(!this.password){
    await passalert.present();
   }else{
    let headers1 = new HttpHeaders();
    // this.localStorageService.setItem('userId', 'tset log');
    // this.router.navigate(['/landing'], { replaceUrl: true, }).then(() => {
    //   this.username='';
    //   this.password='';
    // });
    let headers = new HttpHeaders({
            
      'UserName':this.username,
       
      'Password':this.password,
      
    });
     var data ={
           
          "UserName": this.email,
          "Password": this.password
           //this.password
         }
    
    this._baseServeice.log('',data,headers1).subscribe((res:any) => {
      console.log(res);
      console.log('res',res.Table);
    }, (e:any) => {
     // this.presentToast(e.error.message);
    })
   }
  }
}
