import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Observable } from 'rxjs';
import { LocalStorageService } from 'src/app/core/utils/local-storage.service';
import { SessionData } from 'src/app/core/utils/sessions-keys';

@Injectable({
  providedIn: 'root'
})
export class LandingpageGuard implements CanActivate {
  constructor(
    private platform: Platform,
    private router: Router,
    private localStorageService: LocalStorageService
  ) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    let ret=true;
    this.localStorageService.getItem('userId').then(data => {
      //console.log('1 ',data);
      if (!data || data.length === 0) {
        //console.log('2 ',data);
        this.router.navigate(['/login'], {
          replaceUrl: true
        });
        
        ret=false;
      }
    });
    return ret;
  }
  
}
