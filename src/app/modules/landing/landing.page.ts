import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    
  }
  btnClick(){
//     this.psddataService.setChooseOrgSelection('grower');
//     if(this.growerOrganiserPage_check){
// this.presentAlert();
//     }else{
//       this.psddataService.setGrowerOrganiserPage('');
//       this.router.navigate(['/landing/home'], { replaceUrl: true });
//     }
    this.router.navigate(['/landing/home'], { replaceUrl: true });
  }
}
