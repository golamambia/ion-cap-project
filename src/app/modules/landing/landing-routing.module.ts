import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LandingPage } from './landing.page';
import { LandingpageGuard } from './landingpage.guard';

const routes: Routes = [
  {
    path: '',
    component: LandingPage,
    canActivate: [LandingpageGuard],
    children: [
      {
        canActivate: [LandingpageGuard],
        path: 'home',
        loadChildren: () => import('./home-page/home-page.module').then(m => m.HomePagePageModule)
      },
             
      {
        canActivate: [LandingpageGuard],
        path: '',
        loadChildren: () => import('./home-page/home-page.module').then(m => m.HomePagePageModule)
      }
    ]
  },
   
  // {
  //   path: 'home-page',
  //   loadChildren: () => import('./home-page/home-page.module').then( m => m.HomePagePageModule)
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LandingPageRoutingModule {}
