import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './components/menu/menu.component';
import { SelectDateComponent } from './components/select-date/select-date.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [MenuComponent,SelectDateComponent],
  exports:[MenuComponent,SelectDateComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    FormsModule ,
  ]
})
export class SharedModule { }
