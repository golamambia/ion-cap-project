import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { LocalStorageService } from 'src/app/core/utils/local-storage.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  constructor(private menu: MenuController,private localStorage: LocalStorageService,private router: Router,) { }

  ngOnInit() {}
  logout(){
    this.localStorage.clearAll().then(() => {
      //this.authToken = '';
      //this.authTokenData = '';
      //this.refreshTokenKey = '';
      this.router.navigate(['/login'], { replaceUrl: true });
    });
    this.menu.close();
  }
}
