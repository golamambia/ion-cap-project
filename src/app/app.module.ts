import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { LocalStorageService } from './core/utils/local-storage.service';
import { AuthenticationGuard } from './modules/login/authentication.guard';
import { LandingpageGuard } from './modules/landing/landingpage.guard';
import { BaseService } from './core/base.service';
import { Storage  } from '@ionic/storage';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    CoreModule,
   // Ng2SearchPipeModule,
    SharedModule,
  ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    BaseService,LocalStorageService,Storage,
    AuthenticationGuard,LandingpageGuard
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
