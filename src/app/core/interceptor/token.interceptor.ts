import { Injectable } from '@angular/core';


import { BehaviorSubject, Observable, throwError } from 'rxjs';

import { catchError, switchMap, finalize } from 'rxjs/operators';
import { BaseService } from '../base.service';
import {
    HttpClient,HttpRequest, HttpInterceptor, HttpHandler, HttpSentEvent,
    HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent, HttpErrorResponse
} from '@angular/common/http';
import { environment } from 'src/environments/environment';
//import { urlspaths } from '../services/url-path';
import { LocalStorageService } from '../utils/local-storage.service';
import { SessionData } from '../utils/sessions-keys'; 
import { Router } from '@angular/router';
import { Storage } from '@capacitor/storage';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
 authToken:any='';
    constructor(private authService: BaseService,
        private localStorage: LocalStorageService,
        private router: Router,public http: HttpClient) {
             
              }

    isRefreshingToken: boolean = false;
    tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent |
        HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {

        return next.handle(this.addTokenToRequest(request, this.authService.getAuthToken()))
            .pipe(
                catchError(err => {
                    if (err instanceof HttpErrorResponse) {
                        switch (err.status) {
                            case 481:
                                return this.handle401Error(request, next);
                            case 440:
                                return <any>this.authService.logout();
                            default:
                                return throwError(err);
                        }
                    } else {
                        return throwError(err);
                    }
                }));
    }

    private addTokenToRequest(request: HttpRequest<any>, token): HttpRequest<any> {
   
        if (token != undefined && token) {
           token=JSON.parse(token);
         
           return request.clone({
                setHeaders: {
                 'auth_token':token.authToken,
               'user_login_id':token.userLoginId,
                'org_code':token.orgCode,
                 'languageId':'3',
                 'orgId':token.orgCode,
                  'orgGroup':token.orgGroup,
                }
            });
        
        }else{
            return request; 
        }
        
    }

  

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {

        // if (!this.isRefreshingToken) {
        this.isRefreshingToken = true;

        // Reset here so that the following requests wait until the token
        // comes back from the refreshToken call.
        this.tokenSubject.next(null);

        return this.authService.refreshToken()
            .pipe(
                switchMap((user: any) => {
                    if (user) {
                        this.tokenSubject.next(user.data.token);
                        // localStorage.setItem(SessionData.AUTH_TOKEN, JSON.stringify(user.data.token));
                        return next.handle(this.addTokenToRequest(request, user.data.token));
                    }

                    return <any>this.authService.logout();
                }),
                catchError(() => {
                    return <any>this.authService.logout();
                }),
                finalize(() => {
                    this.isRefreshingToken = false;
                })
            );
        // } else {
        //     this.isRefreshingToken = false;

        //     return this.tokenSubject
        //         .pipe(filter(token => token != null),
        //             take(4),
        //             switchMap(token => {
        //                 return next.handle(this.addTokenToRequest(request, token));
        //             }));
        // }
    }
    async   getAuthToken() {
        //
        const urltogo  =await Storage.get({  key: SessionData.AUTH_TOKEN});
        var authToken=urltogo.value;
        //return this.localStorage.getItem(SessionData.AUTH_TOKEN);
         Storage.get({ key: SessionData.AUTH_TOKEN }).then(token => {
           
           // console.log(token);
            if(token.value){
                //this.authToken = token.value;
            }
           // return this.authToken;
         });
          
       // console.log(authToken);
          return authToken;
        
        //return 123555;
    }
     
}