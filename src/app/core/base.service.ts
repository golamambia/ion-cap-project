import { Injectable } from '@angular/core';
import { LocalStorageService } from './utils/local-storage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, from, Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { SessionData } from './utils/sessions-keys';

@Injectable({
  providedIn: 'root',
})
export class BaseService {
  baseUrl: string = 'https://testingcashupapi.wenodo.com/api/LoginAPI/';
  
  authToken: any = '';
  authTokenData: any = '';
  refreshTokenKey: any = '';

  constructor(
    private _httpClient: HttpClient,
    private localStorage: LocalStorageService,
    private router: Router,
    // private psddataService: PsddataService,
   // private storage: Storage,
    // private network: Network,
    private toastController: ToastController,
    private platform: Platform,
    //private sqlitePorter: SQLitePorter, private sqlite: SQLite,
    // private http: HttpClient,
    private activatedRoute: ActivatedRoute
  ) {
    //this.setAuthKeys();
  }

  post(url: string,body?: any,headersData?: HttpHeaders,token?: string,params?: HttpParams,key?: string): Observable<any> {
    headersData = headersData?.append('Content-Type', 'application/json');
    //console.log(headersData)

    return this._httpClient
      .post(this.baseUrl + 'api/', body, {
        params,
        headers: headersData,
        observe: 'response',
      })
      .pipe(
        map((response) => {
          return <any>response.body;
        }),
        catchError((e) => {
          return throwError(this.getException(e, key));
        })
      );
  }
  log(
    url: string,
    body?: any,
    headersData?: HttpHeaders,
    token?: string,
    params?: HttpParams,
    key?: string
  ): Observable<any> {
    let options: any = {};
   // console.log('params', headersData);
   
   //headersData = headersData?.append('Content-Type', 'application/json');
    let bUrl = this.baseUrl + 'USER_LOGIN';
    return this._httpClient
      .post(bUrl, body, { params, headers: headersData, observe: 'response' })
      .pipe(
        map((response) => {
          return <any>response.body;
        }),
        catchError((e) => {
          return throwError(this.getException(e, key));
        })
      );
  }
  async setAuthKeys() {
    //this.authToken = localStorage.getItem(SessionData.AUTH_TOKEN);

    this.localStorage.getItem(SessionData.AUTH_TOKEN).then((data) => {
      //console.log(data.length);
      if (data && data.length > 0) {
        this.authToken = data;
      }
    });

    this.localStorage.getItem(SessionData.AUTH_DATA_KEY).then((data) => {
      //console.log(data.length);
      if (data && data.length > 0) {
        this.authTokenData = JSON.parse(data);
      }
    });
  }

  logout() {
    this.localStorage.clearAll().then(() => {
      this.authToken = '';
      this.authTokenData = '';
      this.refreshTokenKey = '';
      this.router.navigate(['/login'], { replaceUrl: true });
    });
  }
  getAuthToken() {
    return this.authTokenData ? this.authTokenData : '';
  }
  getException(e: any, key?: string) {
    if (navigator.onLine) {
      if (e && e.error && e.error.message) {
        return e;
      } else {
        return {
          status: -1,
          error: {
            error: e,
            title: 'Oh oh!',
            message:
              'An error occured while fetching the details. Please try after sometime.',
          },
        };
      }
    } else {
      return {
        status: -1,
        error: {
          title: 'Oh oh! No Internet!',
          message: 'Check your internet connection.',
        },
      };
    }
  }
  bankList = [
    { bank: 'Axis Bank Ltd', ifs: 'UTIB' },
    { bank: 'Bandhan Bank Ltd', ifs: 'BDBL' },
    { bank: 'CSB Bank Limited', ifs: 'CSBK' },
    { bank: 'City Union Bank Ltd', ifs: 'CIUB' },
    { bank: 'DCB Bank Ltd', ifs: 'DCBL' },
    { bank: 'Dhanlaxmi Bank Ltd', ifs: 'DLXB' },
    { bank: 'Federal Bank Ltd', ifs: 'FDRL' },
    { bank: 'HDFC Bank Ltd', ifs: 'HDFC' },
    { bank: 'ICICI Bank Ltd', ifs: 'ICIC' },
    { bank: 'IndusInd Bank Ltd', ifs: 'INDB' },
    { bank: 'IDFC FIRST Bank Limited', ifs: 'IDFB' },
    { bank: 'Jammu & Kashmir Bank Ltd', ifs: 'JAKA' },
    { bank: 'Karnataka Bank Ltd', ifs: 'KARB' },
    { bank: 'Karur Vysya Bank Ltd', ifs: 'KVBL' },
    { bank: 'Kotak Mahindra Bank Ltd', ifs: '	KKBK' },
    { bank: 'Nainital bank Ltd', ifs: 'NTBL' },
    { bank: 'RBL Bank Ltd', ifs: 'RATN' },
    { bank: 'South Indian Bank Ltd', ifs: 'SIBL' },
    { bank: 'Tamilnad Mercantile Bank Ltd', ifs: 'TMBL' },
    { bank: 'YES Bank Ltd', ifs: 'YESB' },
    { bank: 'IDBI Bank Limited', ifs: 'IBKL' },
    { bank: 'Bank of Baroda', ifs: 'BARB' },
    { bank: 'Bank of India', ifs: 'BKID' },
    { bank: 'Bank of Maharashtra', ifs: 'MAHB' },
    { bank: 'Canara Bank', ifs: 'CNRB' },
    { bank: 'Central Bank of India', ifs: 'CBIN' },
    { bank: 'Indian Bank', ifs: 'IDIB' },
    { bank: 'Indian Overseas Bank', ifs: 'IOBA' },
    { bank: 'Punjab & Sind Bank', ifs: 'PSIB' },
    { bank: 'Punjab National Bank', ifs: 'PUNB' },
    { bank: 'State Bank of India', ifs: 'SBIN' },
    { bank: 'UCO Bank', ifs: 'UCBA' },
    { bank: 'Union Bank of India', ifs: 'UBIN' },
  ];
}
