export class InputValidator {

   static unwantedCharFilter(input): any {
      if (input.pristine) {
         return null;
      }
      // Warranty Jile 57: Removing , / ; from regEx `,\'";\/\\
      let UNWANTED_CHAR_REGEXP: any = '`\'"\\';
      var getReg = [];
      getReg = UNWANTED_CHAR_REGEXP.split('');
      input.markAsTouched();
      var ar = [];
      ar = input.value.split('');
      const found = getReg.some(element => {
         return ar.includes(element)
      });
      if (!found) {
         return null;
      }
      return {
         invalidChar: true
      };
   }
}