
export class SessionData {
  public static AppStoreDataAuthKey = 'seedsynce-app-database';
  public static AUTH_DATA_KEY = 'auth-data';
  public static AUTH_TOKEN = 'auth-data-token';
  public static AUTH_REFRESH = 'auth-data-refresh-token';
  public static CURRENTDATE = '8ce168b1-27d2-4202-bc45-a17c398cc0fb';

}